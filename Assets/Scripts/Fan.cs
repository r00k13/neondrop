﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Fan : MonoBehaviour
{   
    public Text ft;
    public int fanNumber;

    /*private int direction = 1;
    public float speed = 50f;

    void Start()
    {
        PickDirection();
    }*/

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, Time.deltaTime * 6, 0);
        //transform.Rotate(Vector3.forward, speed * Time.deltaTime * direction);

        if (transform.position.y > 15)
        {
            Vector3 startPos = new Vector3(0, -27, 0);
            transform.position = startPos;
            fanNumber += 2;
            ft.text = "" + fanNumber;
            //PickDirection();
        }
    }

    /*void PickDirection()
    {
        if (Random.Range(0, 2) > 0)
        {
            direction = -1;
        }
    }*/
}