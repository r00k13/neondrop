﻿using UnityEngine;
using System.Collections;

public class FollowObject : MonoBehaviour 
{
	public int height;							//y distance from target
	public int distance;						//z distance from target
	
	public GameObject player;					//target
	 
	void Start () 
	{
		Vector3 location = new Vector3(player.transform.position.x, player.transform.position.y + height, player.transform.position.z + distance);	//location object should be at
		transform.position = location;
	}
	
	void Update () 
	{
		Vector3 location = new Vector3(player.transform.position.x, player.transform.position.y + height, transform.position.z);	//location object should be at
		transform.position = location;
	}
}
