﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public int score = 0;
    public int obstacleNo = 0;

    public GameObject canvasEnd;
    public Text resultsText;
    public Text[] scoreText;
    //public GameObject red;

    public ParticleSystem particles;

    private string[] deathCauses = new string[6]
        {
            "being reduced to a fine mist",
            "parting ways with your limbs",
            "departing this mortal coil",
            "suddenly stopping",
            "finding one you couldn't",
            "painting the walls red"
        };

    public bool paused = false;

    private AudioSource music;

    void Start()
    {
        music = Camera.main.GetComponent<AudioSource>();
        canvasEnd.SetActive(false);
        //red.SetActive(false);

        #if UNITY_ANDROID

        Application.targetFrameRate = 30;

        QualitySettings.vSyncCount = 0;

        QualitySettings.antiAliasing = 0;

        QualitySettings.shadowCascades = 0;
        QualitySettings.shadowDistance = 15;

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        #endif
    }

    void Update()
    {
        if(canvasEnd.activeInHierarchy)
        {
            if (Input.GetButtonDown("Submit"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

            if (Input.GetButtonDown("Cancel"))
            {
                SceneManager.LoadScene(0);
            }
        }
        
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Obstacle" && !paused)
        {
            //red.SetActive(true);
            Destroy(collision.gameObject);
            particles.Pause();
            SetHiScore(score);
            paused = true;
            music.Stop();
            canvasEnd.SetActive(true);
            resultsText.text = "You dodged " + score + " spinning blades before " + deathCauses[Random.Range(0, deathCauses.Length)];
        }
    }

    void SetHiScore(int myScore)
    {
        string scoreKey = "HScore";

        int newScore = myScore;
        int oldScore;

        for (int i = 0; i < scoreText.Length; i++)
        {
            if (PlayerPrefs.HasKey(scoreKey + i))
            {
                if (PlayerPrefs.GetInt(scoreKey + i) < newScore)
                {
                    oldScore = PlayerPrefs.GetInt(scoreKey + i);

                    PlayerPrefs.SetInt(scoreKey + i, newScore);
                    newScore = oldScore;
                }
            }
            else
            {
                PlayerPrefs.SetInt(scoreKey + i, newScore);
                newScore = 0;
            }
        }
        

        ShowHiScores();
    }

    void ShowHiScores()
    {
        string scoreKey = "HScore";

        for (int i = 0; i < scoreText.Length; i++)
        {
            scoreText[i].text = "" + PlayerPrefs.GetInt(scoreKey + i);
        }
    }
}
