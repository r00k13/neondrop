﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{
    public GameObject canvasMenu;
    public GameObject canvasOptions;
    public GameObject canvasScores;
    public GameObject canvasAbout;

    public Toggle invertY;
    public Text[] scoreText;

    public EventSystem es;
    public GameObject defaultMenu;
    public GameObject defaultOptions;
    public GameObject defaultScores;
    public GameObject defaultAbout;

    private AudioSource click;

    void Start()
    {
        Cursor.visible = true;
        click = gameObject.GetComponent<AudioSource>();
		Time.timeScale = 1.0f;

        string scoreKey = "HScore";

        for (int i = 0; i < scoreText.Length; i++)
        {
            scoreText[i].text = "" + PlayerPrefs.GetInt(scoreKey + i);
        }

        if(PlayerPrefs.GetInt("InvertY") == 1 ? true : false)
        {
            invertY.isOn = true;
        }

        //es.SetSelectedGameObject(defaultMenu, new BaseEventData(es));
    }

    public void ShowMenu()
    {
        if (click != null)
        {
            click.Play();
        }

        //es.firstSelectedGameObject = defaultMenu;
        //es.SetSelectedGameObject(defaultMenu, new BaseEventData(es));

        canvasMenu.SetActive(true);
        canvasOptions.SetActive(false);
        canvasScores.SetActive(false);
        canvasAbout.SetActive(false);

        
    }

    public void Play()
    {
        if(click != null)
        {
            click.Play();
        }
        canvasMenu.SetActive(false);
        SceneManager.LoadScene(1);
    }

    public void Options()
    {
        if (click != null)
        {
            click.Play();
        }
        canvasMenu.SetActive(false);
        canvasOptions.SetActive(true);

        //es.SetSelectedGameObject(defaultOptions, new BaseEventData(es));
    }

    public void Scores()
    {
        if (click != null)
        {
            click.Play();
        }
        canvasMenu.SetActive(false);
        canvasScores.SetActive(true);

        //es.SetSelectedGameObject(defaultScores, new BaseEventData(es));
    }

    public void About()
    {
        if (click != null)
        {
            click.Play();
        }
        canvasMenu.SetActive(false);
        canvasAbout.SetActive(true);

        //es.SetSelectedGameObject(defaultAbout, new BaseEventData(es));
    }

    public void Quit()
    {
        if (click != null)
        {
            click.Play();
        }
        Application.Quit();
    }

    public void SetInverted(bool b)
    {
        PlayerPrefs.SetInt("InvertY", b ? 1 : 0);
    }
}
