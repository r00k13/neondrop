﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCanvas : MonoBehaviour
{
    public int tunnelTop = 22;
    private GameManager gm;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("Player").GetComponent<GameManager>();
    }

    void Update()
    {
        if (!gm.paused)
        {
            transform.Translate(0, Time.deltaTime * 6, 0);

            if (transform.position.y > tunnelTop)
            {
                Destroy(gameObject);
            }
        }
    }
}
