﻿using UnityEngine;
using System.Collections;

public class MoveHero : MonoBehaviour 
{
    public float speed = 400;
    public float maxSpeed = 600;

    private Rigidbody rb;

    private float startX;
    private float startY;

    private int invertY = 1;

    private GameManager gm;

    // Use this for initialization
    void Start () 
	{
        rb = GetComponent<Rigidbody>();
        gm = GetComponent<GameManager>();
        startX = Input.acceleration.x;
        startY = Input.acceleration.y;

        if(PlayerPrefs.GetInt("InvertY") == 1)
        {
            invertY = -1;
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () 
	{
        if(!gm.paused)
        {
            if (Input.GetAxis("Vertical") > 0 || Input.acceleration.y > startY)                     //if w or s pressed, move up or down
            {
                rb.AddForce(((new Vector3(0, 0, Time.deltaTime) * speed) - rb.velocity) * invertY);
            }
            else if (Input.GetAxis("Vertical") < 0 || Input.acceleration.y < startY)
            {
                rb.AddForce(((new Vector3(0, 0, Time.deltaTime * -1) * speed) - rb.velocity) * invertY);
            }

            if (Input.GetAxis("Horizontal") > 0 || Input.acceleration.x > startX)                       //if w or s pressed, move up or down
            {
                rb.AddForce((new Vector3(Time.deltaTime, 0, 0) * speed));
            }
            else if (Input.GetAxis("Horizontal") < 0 || Input.acceleration.x < startX)
            {
                rb.AddForce((new Vector3(Time.deltaTime * -1, 0, 0) * speed));
            }

            //rb.AddForce((new Vector3(Input.acceleration.x, 0, Input.acceleration.z)));

            if (Input.GetMouseButton(0))
            {
                Vector3 dir = new Vector3(Input.mousePosition.x, 0, Input.mousePosition.y) - new Vector3(Screen.width / 2, 0, Screen.height / 2);
                rb.AddForce(dir.normalized * speed * Time.deltaTime);
            }

            rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
        }
        else
        {
            rb.velocity = new Vector3(0, 0, 0);
        }   
    }

	void OnCollisionEnter (Collision collision)
	{
		if (collision.gameObject.tag == "Wall") 
		{
            Camera.main.GetComponent<ScreenShake>().ShakeScreen(0.4f, 1.2f);
            rb.AddForce((transform.position - collision.contacts[0].point) * 500);
            Debug.Log("Hit Wall");
        }
	}
}
