﻿using UnityEngine;
using System.Collections;

public class MoveNeon : MonoBehaviour
{
    public int tunnelTop = 22;
    private GameManager gm;

    void Start()
    {
        transform.Rotate(Vector3.up * Random.Range(0, 360));

        gm = GameObject.FindGameObjectWithTag("Player").GetComponent<GameManager>();
    }

    void Update()
    {
        if(!gm.paused)
        {
            transform.Translate(0, Time.deltaTime * 6, 0);

            if (transform.position.y > tunnelTop)
            {
                Destroy(gameObject);
            }
        }
    }
}
