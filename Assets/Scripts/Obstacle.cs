﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Obstacle : MonoBehaviour
{
    public Text ft;
    private int obsNo;
    public int tunnelTop = 22;
    private GameManager gm;

    void Start()
    {
        transform.Rotate(Vector3.up * Random.Range(0, 360));

        gm = GameObject.FindGameObjectWithTag("Player").GetComponent<GameManager>();
        gm.obstacleNo++;
        obsNo = gm.obstacleNo;
        ft.text = "" + obsNo;
    }

    void Update()
    {
        if (!gm.paused)
        {
            transform.Translate(0, Time.deltaTime * 6, 0);

            if (transform.position.y > tunnelTop)
            {
                gm.score = obsNo;
                Destroy(gameObject);
            }
        }
    }
}
