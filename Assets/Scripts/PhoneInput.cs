﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PhoneInput : MonoBehaviour
{
    public Text x;
    public Text y;
    public Text z;


	// Update is called once per frame
	void Update ()
    {
        x.text = "" + Input.acceleration.x;
        y.text = "" + Input.acceleration.y;
        z.text = "" + Input.acceleration.z;
    }
}
