﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
    private float rotationSpeed;

    private GameManager gm;

    //public GameObject trailsClockwise;
    //public GameObject trailsAnticlockwise;

    void Start()
    {
        rotationSpeed = Random.Range(50, 85);
        if(Random.Range(0, 2) == 0)
        {
            rotationSpeed *= -1;
        }

        //if(rotationSpeed > 0)
        //{
        //    trailsClockwise.SetActive(true);
        //    trailsAnticlockwise.SetActive(false);
        //}
        //else
        //{
        //    trailsClockwise.SetActive(false);
        //    trailsAnticlockwise.SetActive(true);
        //}

        gm = GameObject.FindGameObjectWithTag("Player").GetComponent<GameManager>();
    }

	void Update () 
	{
        if (!gm.paused)
        {
            transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);		//rotate around y-axis
        }
    }
}
