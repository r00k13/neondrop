﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour 
{
	private Camera cam;
	
	private float viewX = 0f;
	private float viewY = 0f;
	
	public float xIntensity = 0.05f;
	public float yIntensity = 0.05f;
	public float xRate = 1f;
	public float yRate = 0.6f;
	private float intensityMultiplier = 1f;

	private float duration = 1f;
	private float shakeStart = 0f;
	private bool shaking = false;
	
	// Use this for initialization
	void Start () 
	{
		cam = gameObject.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Y))
		{
			ShakeScreen(1.0f, 100f);
		}

		if(shaking)
		{
			viewX = (Time.unscaledDeltaTime * -1f * ((xIntensity * intensityMultiplier)/2f) + Mathf.PingPong(xRate * intensityMultiplier, xIntensity * intensityMultiplier));
			viewY = (Time.unscaledDeltaTime * -1f * ((yIntensity * intensityMultiplier)/2f) + Mathf.PingPong(yRate * intensityMultiplier, yIntensity * intensityMultiplier));
			
			OffsetCameraObliqueness(viewX, viewY);

            if(intensityMultiplier > 0.5f)
            {
                intensityMultiplier -= Time.unscaledDeltaTime;
            }
			
			if(Time.realtimeSinceStartup > shakeStart + duration)
			{
				shaking = false;
			}
		}
	}
	
	public void ShakeScreen(float d, float i)
	{
        //if(!shaking)
        //{
            duration = d;
            shakeStart = Time.realtimeSinceStartup;
            shaking = true;
            intensityMultiplier = Mathf.Clamp(i, 0.5f, 250f);
        //}
		
	}
	
	void OffsetCameraObliqueness (float xOffset, float yOffset) 
	{
		float frustrumHeight = 2 * cam.nearClipPlane * Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);
		float frustrumWidth = frustrumHeight * cam.aspect;
		Matrix4x4 mat = cam.projectionMatrix;
		mat[0, 2] = 2 * xOffset / frustrumWidth;
		mat[1, 2] = 2 * yOffset / frustrumHeight;
		cam.projectionMatrix = mat;
	}
}
