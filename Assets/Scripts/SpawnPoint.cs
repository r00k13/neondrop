﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour
{
    public GameObject[] prefabs;                                                    //objects that can be spawned
    public float interval = 2.0f;                                                   //delay between spawnings
    private float nextSpawn = 0f;                                                   //time next spawn is due

    void Update()
    {
        if (Time.time > nextSpawn)                                                  //if a spawn is due
        {
            nextSpawn = Time.time + interval;                                       //set time next spawn will happen

            SpawnObject(Random.Range(0, prefabs.Length), transform.position);
        }
    }

    void SpawnObject(int type, Vector3 location)
    {
        Instantiate(prefabs[type], location, prefabs[type].transform.rotation);     //spawn the object
    }
}
